<?php declare(strict_types=1);

namespace ForwardMX\ValueObject;

class DomainAliases
{
    /** @var Domain */
    private $domain;

    /** @var Alias[] */
    private $aliases;

    /**
     * @param Domain $domain
     * @param Alias[] $aliases
     */
    public function __construct(Domain $domain, array $aliases)
    {
        $this->domain = $domain;
        $this->aliases = $aliases;
    }

    public function getDomain(): Domain
    {
        return $this->domain;
    }

    /**
     * @return Alias[]
     */
    public function getAliases(): array
    {
        return $this->aliases;
    }
}
