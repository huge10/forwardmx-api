<?php declare(strict_types=1);

namespace ForwardMX;

use ForwardMX\Exception\ForwardMxException;
use ForwardMX\ValueObject\Alias;
use ForwardMX\ValueObject\Domain;
use ForwardMX\ValueObject\DomainAliases;

class ForwardMX
{
    const DOMAIN_LIST_PATH = 'domains/';
    const DOMAIN_CREATE_PATH = 'domain/create/';
    const DOMAIN_REMOVE_PATH = 'domain/destroy/';

    const ALIAS_LIST_PATH = 'aliases/';
    const ALIAS_CREATE_PATH = 'alias/create/';
    const ALIAS_UPDATE_PATH = 'alias/update/';
    const ALIAS_REMOVE_PATH = 'alias/destroy/';

    /** @var string */
    private $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return Domain[]
     * @throws ForwardMxException
     */
    public function getDomains(): array
    {
        $result = [];
        foreach (ForwardMxRequest::makeRequest($this->apiKey, ForwardMX::DOMAIN_LIST_PATH) as $domain) {
            $result[] = new Domain(...array_values($domain));
        }
        return $result;
    }

    /**
     * @return Domain[]
     * @throws ForwardMxException
     */
    public function getDomainsByStatus(string $status): array
    {
        $result = [];
        foreach (ForwardMxRequest::makeRequest($this->apiKey, ForwardMX::DOMAIN_LIST_PATH) as $domain) {
            if ($domain['status'] === $status) {
                $result[] = new Domain(...array_values($domain));
            }
        }
        return $result;
    }


    /**
     * @throws ForwardMxException
     */
    public function getDomain(string $domain): Domain
    {
        foreach (ForwardMxRequest::makeRequest($this->apiKey, ForwardMX::DOMAIN_LIST_PATH) as $item) {
            if ($item['domain'] === $domain) {
                return new Domain(...array_values($item));
            }
        }
        throw ForwardMxException::domainNotFound($domain);
    }

    /**
     * @throws ForwardMxException
     */
    public function createDomain(string $domain): void
    {
        ForwardMxRequest::makeRequest($this->apiKey, ForwardMX::DOMAIN_CREATE_PATH, ['domain' => $domain]);
    }

    /**
     * @throws ForwardMxException
     */
    public function removeDomain(string $domain): void
    {
        ForwardMxRequest::makeRequest($this->apiKey, ForwardMX::DOMAIN_REMOVE_PATH, ['domain' => $domain]);
    }

    /**
     * @return Alias[]
     * @throws ForwardMxException
     */
    public function getAliases(string $domain): array
    {
        $result = [];
        foreach (ForwardMxRequest::makeRequest($this->apiKey, ForwardMX::ALIAS_LIST_PATH, ['domain' => $domain]) as $alias) {
            $result[] = new Alias(...array_values($alias));
        }
        return $result;
    }

    /**
     * @return DomainAliases[]
     * @throws ForwardMxException
     */
    public function getAllAliases(): array
    {
        $result = [];
        foreach ($this->getDomains() as $domain) {
            $result[] = new DomainAliases($domain, $this->getAliases($domain->getDomain()));
        }
        return $result;
    }

    /**
     * @return Alias[]
     * @throws ForwardMxException
     */
    public function getAliasesByDestination(string $domain, string $destination): array
    {
        $result = [];
        foreach (ForwardMxRequest::makeRequest($this->apiKey, ForwardMX::ALIAS_LIST_PATH, ['domain' => $domain]) as $alias) {
            if ($alias['destination' === $destination]) {
                $result[] = new Alias(...array_values($alias));
            }
        }
        return $result;
    }

    /**
     * @return DomainAliases[]
     * @throws ForwardMxException
     */
    public function getAllAliasesByDestination(string $destination): array
    {
        $result = [];
        foreach ($this->getDomains() as $domain) {
            $result[] = new DomainAliases($domain, $this->getAliasesByDestination($domain->getDomain(), $destination));
        }
        return $result;
    }

    /**
     * @throws ForwardMxException
     */
    public function getAlias(string $domain, string $alias): Alias
    {
        foreach (ForwardMxRequest::makeRequest($this->apiKey, ForwardMX::ALIAS_LIST_PATH, ['domain' => $domain]) as $item) {
            if ($item['source'] === $alias) {
                return new Alias(...array_values($item));
            }
        }
        throw ForwardMxException::aliasNotFound($alias, $domain);
    }

    /**
     * @throws ForwardMxException
     */
    public function createAlias(string $domain, string $alias, string $destination): void
    {
        ForwardMxRequest::makeRequest($this->apiKey, ForwardMX::ALIAS_CREATE_PATH, [
            'domain' => $domain,
            'alias' => $alias,
            'destination' => $destination
        ]);
    }

    /**
     * @throws ForwardMxException
     */
    public function createAliasForAllDomains(string $alias, string $destination): void
    {
        foreach ($this->getDomains() as $domain) {
            $this->createAlias($domain->getDomain(), $alias, $destination);
        }
    }

    /**
     * @throws ForwardMxException
     */
    public function updateAlias(string $domain, string $alias, string $destination): void
    {
        ForwardMxRequest::makeRequest($this->apiKey, ForwardMX::ALIAS_UPDATE_PATH, [
            'domain' => $domain,
            'alias' => $alias,
            'destination' => $destination
        ]);
    }

    /**
     * @throws ForwardMxException
     */
    public function removeAlias(string $domain, string $alias): void
    {
        ForwardMxRequest::makeRequest($this->apiKey, ForwardMX::ALIAS_REMOVE_PATH, [
            'domain' => $domain,
            'alias' => $alias
        ]);
    }

    /**
     * @throws ForwardMxException
     */
    public function removeAliasForAllDomains(string $alias): void
    {
        foreach ($this->getDomains() as $domain) {
            $this->removeAlias($domain->getDomain(), $alias);
        }
    }

    /**
     * @throws ForwardMxException
     */
    public function removeAllAliasesForDomain(string $domain): void
    {
        foreach ($this->getAliases($domain) as $alias) {
            if ($alias->getAlias() !== '') {
                $this->removeAlias($domain, $alias->getAlias());
            }
        }
    }
}
