<?php declare(strict_types=1);

namespace ForwardMX\Exception;

use Exception;

class ForwardMxException extends Exception
{
    public static function curlError(string $message): ForwardMxException
    {
        return new ForwardMxException("ForwardMx connection error: \"${message}\".");
    }

    public static function error(string $error): ForwardMxException
    {
        return new ForwardMxException($error);
    }

    public static function domainNotFound(string $domain): ForwardMxException
    {
        return new ForwardMxException("Domain \"${$domain}\" not found!");
    }

    public static function aliasNotFound(string $alias, string $domain): ForwardMxException
    {
        return new ForwardMxException("Alias \"${alias}\" not found in ${domain}!");
    }
}
