<?php declare(strict_types=1);

namespace ForwardMX;

use ForwardMX\Exception\ForwardMxException;

final class ForwardMxRequest
{
    /** @var string */
    const URL = 'https://forwardmx.io/api/';


    /**
     * @throws ForwardMxException
     */
    public static function makeRequest(string $apiKey, string $path, array $data = []): array
    {
        $data['key'] = $apiKey;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, ForwardMxRequest::URL . $path);

        $result = curl_exec($curl);

        if ($result === false) {
            throw  ForwardMxException::curlError(curl_error($curl));
        }
        $arrayResult = json_decode($result, true);
        if ($arrayResult === null) {
            $arrayResult = [];
        }
        if (isset($arrayResult['error'])) {
            throw ForwardMxException::error($arrayResult['error']);
        }

        curl_close($curl);

        return $arrayResult;
    }
}
