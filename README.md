# ForwardMx Api

Composer repository for the [ForwardMX](https://forwardmx.io/) API.

## Getting started

Firstly, you need to install this library with [Composer](https://getcomposer.org/):

```
composer require wiesner/forwardmx-api
```

## Usage Example

To use ForwardMX, you need to construct a ForwardMX class with Api Key as a constructor parameter.

```php
<?php

use ForwardMX\Exception\ForwardMxException;
use ForwardMX\ForwardMX;
use ForwardMX\ValueObject\Domain;

/**
* @return Domain[]
* @throws ForwardMxException
*/
function getAllDomains(): array
{
    $api = new ForwardMX('api-key');
    
    return $api->getDomains();
}

?>
```

### List of All possible methods

To get any information from ForwardMx use these methods:

| Method      | Return | 
| ----------- | ----------- |
| `getDomains()`      | Array of Domain object       |
| `getDomainsByStatus(string $status)`   | Array of Domain object        |
| `getDomain(string $domain)` | Domain object (throws exception if domain is not found) |
| `getAliases(string $domain)` | Array of Alias object |
| `getAllAliases()` | Array of DomainAlias object |
| `getAliasesByDestination(string $domain, string $destination)` | Array of Alias object |
| `getAllAliasesByDestination(string $destination)` | Array of DomainAliases object |
| `getAlias(string $domain, string $alias)` | Alias object (throws exception if alias is not found) |

To add/change something use these methods:

| Method      | Return | 
| ----------- | ----------- |
| `createDomain(string $domain)` | void |
| `createAlias(string $domain, string $alias, string $destination)` | void |
| `createAliasForAllDomains(string $alias, string $destination)` | void |
| `updateAlias(string $domain, string $alias, string $destination)` | void |

To remove something use these methods:

| Method      | Return | 
| ----------- | ----------- |
| `removeDomain(string $domain)` | void |
| `removeAlias(string $domain, string $alias)` | void |
| `removeAliasForAllDomains(string $alias)` | void |

Notes:

- **string $domain** parameter must be in a format **"some.domain.com"**.
- **string $alias** parameter must be in a format **"some_alias"** _without_ **"@"** at the end of string!
- **string $destination** parameter must be a standard email address like **something@something.com**.

### List of All possible returned objects

some methods return these objects or array of objects.

**ForwardMX\ValueObject\Alias**

| Method      | Return | 
| ----------- | ----------- |
| `getSource()` | string of source (alias with '@') |
| `getDestination()` | string of destination email address |
| `getCount()` | integer of count |


**ForwardMX\ValueObject\Domain**

| Method      | Return |
| ----------- | ----------- |
| `getDomain()` | string of domain (like "something.com") |
| `getStatus()` | string of domain status (like "active") |


**ForwardMX\ValueObject\DomainAliases**

| Method      | Return |
| ----------- | ----------- |
| `getDomain()` | Domain object (see above) |
| `getAliases()` | Array of Alias object (see above) |

## License
This PHP ForwardMx Api SDK is open source software licensed under the [MIT license](https://opensource.org/licenses/MIT). See the [LICENSE](https://gitlab.com/kasner/forwardmx-api/-/blob/baff00bb5797ef605fa9177a03dfc47b59306723/LICENSE.txt) file for more info.

## Epilogue

Special thanks to my wife Patricia <3. Thanks for Your support!
